#! /bin/bash

cd /vopt/
if [ ! -e apache-maven ]; then
  wget --quiet http://www-eu.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
  tar -xf apache-maven-3.5.4-bin.tar.gz
  mv apache-maven-3.5.4/ apache-maven/
fi

update-alternatives --install /usr/bin/mvn maven /vopt/apache-maven/bin/mvn 1001
