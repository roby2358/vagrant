#! /bin/bash

cd /vopt
if [ ! -e spark-2.3.2-bin-hadoop2.7.tgz ]; then
wget --quiet https://www-us.apache.org/dist/spark/spark-2.3.2/spark-2.3.2-bin-hadoop2.7.tgz
tar xvfz spark-2.3.2-bin-hadoop2.7.tgz
fi

cat >> /home/vagrant/.bashrc << WOO
export SPARK_HOME=/vopt/spark-2.3.2-bin-hadoop2.7
export PATH=\$PATH:\$SPARK_HOME/bin
WOO

