#! /bin/bash

add-apt-repository -y ppa:deadsnakes/ppa
apt install -y python3.7 python3-pip
# apt install -y python3.9 python3-pip
# update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
