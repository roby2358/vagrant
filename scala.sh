#! /bin/bash

# sdkman pulls 2.13
apt-get install zip unzip
curl -s "https://get.sdkman.io" | bash
echo '# NOT WORKING, DIR NOT FOUND'
source "/home/vagrant/.sdkman/bin/sdkman-init.sh"
sdk install scala
sdk install sbt

# https://github.com/sbt/sbt/releases/download/v1.4.0/sbt-1.4.0.tgz
# tar xvfz sbt-1.4.0.tgz

# apt-get pulls 2.11
#echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
#apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
#apt-get update
#apt-get install -y sbt
#apt-get install -y scala
#
#cat >> /home/vagrant/.bashrc << WOO
#export SCALA_HOME=/usr/bin/scala
#export PATH=\$PATH:\$SCALA_HOME/bin
#WOO

echo sbt -version
echo scala -version
