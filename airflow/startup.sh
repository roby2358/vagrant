#!/usr/bin/env bash
export AIRFLOW_HOME=~/work/airflow
mkdir -p $AIRFLOW_HOME
pipenv install apache-airflow
echo airflow initdb
echo airflow webserver -p 8080
echo airflow scheduler
echo 'visit localhost:8080 in the browser and enable the example dag in the home page'
